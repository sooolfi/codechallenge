# README #

Codigo que resuelve el challenge planteado por experta.

### ¿cómo ejecutar? ###

1 - clonar el repositorio:


```shell
git clone https://bitbucket.org/sooolfi/codechallenge.git
```
2 - Dentro de la carpeta codechallenge:


```shell
gradle clean assemble
```

3 - Ejecutar app:


```shell
 java -jar build/libs/codeChallenge-1.0-SNAPSHOT.jar
```


Se desarrollo una pequeña interfaz que posibilita interactuar con los ejercicios propuestos
para ir probando.

### Detalle Funciones 1,2,3 ###


Dentro de la clase PatternHelper encontramos 3 funciones que resuelven el punto 1, 2 y 3 del enunciado.

```Java
   /**
     * Retorna una lista con las coincidencias encontradas para
     * los patrones pasados como parametro
     *
     * @param patternList
     * @param chain
     * @return
     */
public static List<String> getCoincidenceList(final List<Pattern> patternList, final String chain);
    
	/**
     * Retorna una lista con las coincidencias encontradas para
     * los patrones pasados como parametro. Cuando un parametro es encontrado,
     * no puede volver a utilizarse la subcadena que produjo la coincidencia
     * @param patternList
     * @param chain
     * @return
     */
public static List<String> getCoincidenceListNotReusable(final List<Pattern> patternList,
                                                             final String chain);
	/**
     * Retorna una lista con las coincidencias encontradas para
     * los patrones pasados como parametro. Cuando un parametro es encontrado,
     * no puede volver a utilizarse la subcadena que produjo la coincidencia.
     * La lista no puede contener cadenas que coincidan con el patron de lista negra.
     *
     *
     * @param patternList
     * @param chain
     * @param blackListPattern
     * @return
     */										 
public static List<String> getCoincidenceListNotReusableBlackList(
            final List<Pattern> patternList, final String chain,
            final Pattern blackListPattern)															 
															 
```

### Detalle Ejercicio 4 ###

Dentro de la clase UrlExtractor encontramos las funciones necesarias para resolver el apartado 4.

```Java

	/**
     * Dada una URL , devuelve el String asociado a la llamada GET a esa URL
     *
     * @return
     * @throws IOException
     */
    public static String getResponseURL(final String url) throws IOException;
	
	/**
     * Imprime el nombre y URL de la APT alojadas en el repositorio GIT de mitre, 
	 * y estan dentro de los dominios de búsqueda pasados como parámetro.
     * @param urlDomains
     * @param url
     * @return
     */
    public static List<String> printAPTurlList(final List<String> urlDomains,
                                               final String url) ;
		
	
```

En el apartado 4 se tomo como supuesto que la estructura del json dentro de los APTs en invariante. y realizando la busqueda sobre el array 
objects en primer medida y luego buscando las URLs dentro de external_references.
Se utilizo la API de github para obtener el listado de archivos dentro de una url determinada.

```JSON
{
    "type": 
    "id": 
    "spec_version": 
    "objects": [
        {
            "created_by_ref": 
            "object_marking_refs": [
               
            ],
            "external_references": [
                {
                    "external_id": 
                    "source_name": 
                    "url": 
                }, 
                
                ...


            ],
            "description": 
            "name": 
            "type": 
            "id": 
            "aliases": [                                
            ],
            "modified": 
            "created": 
            "x_mitre_version":
        }
    ]
}
```




