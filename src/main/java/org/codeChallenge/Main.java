package org.codeChallenge;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Pattern;

public class Main {


    /**
     * Pequeña interfaz que permite interactuar con las funciones propuestas
     * en el challenge
     *
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {

        System.out.println("*******************************************************");
        System.out.println("Seleccione Función \n");
        System.out.println("1 - Descargar URLs APT");
        System.out.println("2 - Probar Funciones Patterns");
        System.out.println("*******************************************************");

        BufferedReader obj = new BufferedReader(new InputStreamReader(System.in));

        boolean checkOption = false;
        Integer seleccion = null;

        while (!checkOption) {

            String str = obj.readLine();

            try {
                seleccion = new Integer(str);

                if (!seleccion.equals(1) && !seleccion.equals(2)) {
                    System.out.println("Debe seleccionar opción 1 o 2");
                } else {
                    checkOption = true;
                }

            } catch (NumberFormatException ex) {
                System.out.println("Debe seleccionar opción 1 o 2");
            }
        }


        if (seleccion.equals(1)) {

            /**
             * Imprimo la lista de ulr solicitada en el ejercicio
             */

            ArrayList<String> arrayDomains = new ArrayList<>();
            arrayDomains.add("cybereason.com");
            arrayDomains.add("symantec.com");
            UrlExtractor.printAPTurlList(arrayDomains, UrlExtractor.URL_REPO_GIT);

        } else {
            /**
             * Selecciono Funcion a probar.
             */
            System.out.println("*******************************************************");
            System.out.println("Seleccione Función Pattern a probar \n");
            System.out.println("1 - Función 1");
            System.out.println("2 - Función 2");
            System.out.println("3 - Función 3");
            System.out.println("*******************************************************");


            checkOption = false;
            while (!checkOption) {

                String str = obj.readLine();

                try {
                    seleccion = new Integer(str);

                    if (seleccion < 1 || seleccion > 3) {
                        System.out.println("Debe seleccionar opción 1, 2 o 3");
                    } else {
                        checkOption = true;
                    }

                } catch (NumberFormatException ex) {
                    System.out.println("Debe seleccionar opción 1, 2 o 3");
                }
            }


            String patternKeyboard = null, chain = null, blackPatterKeyboard = null;
            ArrayList<Pattern> patterns = new ArrayList<>();

            System.out.println("Ingresar Los Patrones separados por un espacio");
            checkOption = false;
            while (!checkOption) {
                patternKeyboard = obj.readLine();
                checkOption = true;
            }
            System.out.println("Ingresar la Cadena");
            checkOption = false;
            while (!checkOption) {
                chain = obj.readLine();
                checkOption = true;
            }
            String[] patternsString = patternKeyboard.split(" ");
            for (String string : patternsString) {
                patterns.add(Pattern.compile(string));
            }


            try {

                switch (seleccion) {
                    case 1:
                        System.out.println("Coincidencias: ");
                        System.out.println(PatternHelper.getCoincidenceList(patterns, chain));
                        break;
                    case 2:
                        System.out.println("Coincidencias: ");
                        System.out.println(PatternHelper.getCoincidenceListNotReusable(patterns, chain));
                        break;

                    case 3:
                        System.out.println("Ingresar El Patron de lista Negra");
                        checkOption = false;
                        while (!checkOption) {
                            blackPatterKeyboard = obj.readLine();
                            checkOption = true;
                        }
                        System.out.println("Coincidencias: ");
                        System.out.println(PatternHelper.getCoincidenceListNotReusableBlackList(patterns, chain,
                                Pattern.compile(blackPatterKeyboard)));

                        break;
                }
            } catch (PatterException ex) {
                System.out.println(ex.getMessage());
            }


        }

    }


}
