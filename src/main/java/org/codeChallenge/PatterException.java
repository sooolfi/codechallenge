package org.codeChallenge;

public class PatterException extends RuntimeException {


    public static final String NULL_CHAIN = "La cadena no puede ser nula.";
    public static final String NULL_BLACKLIST= "El patron de lista negra no puede ser nulo.";


    public PatterException(String message) {
        super(message);
    }
}
