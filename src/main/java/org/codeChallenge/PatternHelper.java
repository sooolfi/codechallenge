package org.codeChallenge;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PatternHelper {


    /**
     * Retorna una lista con las coincidencias encontradas para
     * los patrones pasados como parametro
     *
     * @param patternList
     * @param chain
     * @return
     */
    public static List<String> getCoincidenceList(final List<Pattern> patternList, final String chain) {


        if (patternList == null || patternList.size() == 0) {
            return new ArrayList<>();
        }

        if (chain == null || chain.trim().isEmpty()) {
            throw new PatterException(PatterException.NULL_CHAIN);
        }
        List<String> coincidenceList = new ArrayList<>();
        Matcher matcher;

        for (Pattern p : patternList) {
            matcher = p.matcher(chain);
            while (matcher.find()) {
                coincidenceList.add(matcher.group());
            }

        }

        return coincidenceList;
    }

    /**
     * Retorna una lista con las coincidencias encontradas para
     * los patrones pasados como parametro. Cuando un parametro es encontrado,
     * no puede volver a utilizarse la subcadena que produjo la coincidencia
     * @param patternList
     * @param chain
     * @return
     */
    public static List<String> getCoincidenceListNotReusable(final List<Pattern> patternList,
                                                             final String chain) {
        if (patternList == null || patternList.size() == 0) {
            return new ArrayList<>();
        }

        if (chain == null || chain.trim().isEmpty()) {
            throw new PatterException(PatterException.NULL_CHAIN);
        }

        List<String> coincidenceList = new ArrayList<>();
        List<Pattern> notReusable = new ArrayList<>();

        Matcher matcher;
        for (Pattern p : patternList) {
            matcher = p.matcher(chain);
            while (matcher.find()) {
                if (getCoincidenceList(notReusable, matcher.group()).size() == 0) {
                    coincidenceList.add(matcher.group());
                    notReusable.add(Pattern.compile(matcher.group()));
                }
            }
        }

        return coincidenceList;
    }


    /**
     * Retorna una lista con las coincidencias encontradas para
     * los patrones pasados como parametro. Cuando un parametro es encontrado,
     * no puede volver a utilizarse la subcadena que produjo la coincidencia.
     * La lista no puede contener cadenas que coincidan con el patron de lista negra.
     *
     *
     * @param patternList
     * @param chain
     * @param blackListPattern
     * @return
     */
    public static List<String> getCoincidenceListNotReusableBlackList(
            final List<Pattern> patternList, final String chain,
            final Pattern blackListPattern) {


        if (patternList == null || patternList.size() == 0) {
            return new ArrayList<>();
        }


        if (chain == null || chain.trim().isEmpty()) {
            throw new PatterException(PatterException.NULL_CHAIN);
        }

        if (blackListPattern == null) {
            throw new PatterException(PatterException.NULL_BLACKLIST);
        }

        List<String> coincidenceList = new ArrayList<>();
        List<Pattern> notReusable = new ArrayList<>();

        Matcher matcher;
        for (Pattern p : patternList) {
            matcher = p.matcher(chain);
            while (matcher.find()) {
                if (getCoincidenceList(notReusable, matcher.group()).size() == 0
                        && !blackListPattern.matcher(matcher.group()).find()) {
                    coincidenceList.add(matcher.group());
                    notReusable.add(Pattern.compile(matcher.group()));
                }
            }
        }

        return coincidenceList;
    }

}
