package org.codeChallenge;

import org.apache.http.HttpStatus;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import org.codehaus.jettison.json.JSONArray;

public class UrlExtractor {


    public static final String URL_REPO_GIT = "https://api.github.com/repos/mitre/cti/contents/enterprise-attack/intrusion-set";

    /**
     * Dada una URL , devuelve el String asociado a la llamada GET a esa URL
     *
     * @return
     * @throws IOException
     */
    public static String getResponseURL(final String url) throws IOException {


        if (url == null || url.trim().isEmpty()) {
            throw new UrlExtractorException(UrlExtractorException.INCORRECT_URL);
        }

        ResteasyClient client = new ResteasyClientBuilder().build();
        WebTarget webTarget = client.target(url);
        Response response;

        try {
            response = webTarget.request(MediaType.APPLICATION_JSON_TYPE)
                    .header("User-Agent", "request")
                    .get();

            if (response.getStatus() == HttpStatus.SC_OK) {
                return response.readEntity(String.class);
            } else {
                throw new UrlExtractorException(UrlExtractorException.INCORRECT_RESPONSE);
            }

        } catch (Exception ex) {
            if(ex instanceof UrlExtractorException){
                throw ex;
            }else{
                throw new UrlExtractorException(UrlExtractorException.INCORRECT_GET);
            }

        }

    }

    /**
     * Imprime el nombre y URL de la APT alojadas en el repositorio GIT de mitre,
     * y estan dentro de los dominios de búsqueda pasados como parámetro.
     * @param urlDomains
     * @param url
     * @return
     */
    public static List<String> printAPTurlList(final List<String> urlDomains,
                                               final String url) {

        try {

            JSONArray aptRepoList = new JSONArray(getResponseURL(url));

            /**
             * Recorremos la lista APT para extraer URL y nombre
             */

            JSONObject jsonApt;
            ArrayList<String> result = new ArrayList<>();

            System.out.println("Procesando " + aptRepoList.length() + " APT.");

            for (int n = 0; n < aptRepoList.length(); n++) {

                jsonApt = aptRepoList.getJSONObject(n);
                String urlApt = jsonApt.getString("download_url");
                JSONObject apt = new JSONObject(getResponseURL(urlApt));
                JSONArray arrayAptObtejct = apt.getJSONArray("objects");
                JSONObject jsonObject = (JSONObject) arrayAptObtejct.get(0);
                JSONArray arrayExternalReferences = jsonObject.getJSONArray("external_references");

                for (int i = 0; i < arrayExternalReferences.length(); i++) {
                    JSONObject jsonItem = new JSONObject(arrayExternalReferences.get(i).toString());

                    try {
                        String urlItem = (String) jsonItem.get("url");
                        if (urlItem != null) {
                            //verifico que este dentro de los dominios de busqueda
                            if (urlDomains.stream().anyMatch(urlItem::contains)) {
                                result.add(jsonItem.get("source_name") + " , " + jsonItem.get("url"));
                                System.out.println(jsonItem.get("source_name") + " , " + jsonItem.get("url"));
                            }
                        }

                    } catch (JSONException ex) {
                        //Not Found download_url , objects or external_references json
                    }

                }
            }


            return result;
        } catch (JSONException | IOException e) {
            throw new UrlExtractorException(UrlExtractorException.JSON_ERROR);
        }


    }

}
