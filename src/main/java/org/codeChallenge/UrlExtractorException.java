package org.codeChallenge;

public class UrlExtractorException extends RuntimeException {

    public static final String INCORRECT_GET = "Imposible invocar la llamada.";
    public static final String INCORRECT_RESPONSE = "La petición no se pudo realizar.";
    public static final String INCORRECT_URL = "La url no puede ser nula.";
    public static final String JSON_ERROR= "Error procesando los objetos JSON.";


    public UrlExtractorException(String message) {
        super(message);
    }


}
