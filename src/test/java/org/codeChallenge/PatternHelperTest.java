package org.codeChallenge;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

public class PatternHelperTest {


    @Rule
    public ExpectedException expectedEx = ExpectedException.none();



    @Test
    public void testInitialTest() {
        String chain = "The fox jumped over the fence";
        List<Pattern> patternList = new ArrayList<>();
        patternList.add(Pattern.compile("(the)"));
        patternList.add(Pattern.compile("(fox|f.nce)"));
        List<String> result = PatternHelper.getCoincidenceList(patternList, chain);
        System.out.println(result);
        assertEquals(3, result.size());
        assertEquals("the",result.get(0));
        assertEquals("fox",result.get(1));
        assertEquals("fence",result.get(2));
    }

    @Test
    public void testInitialTest_2() {
        String chain = "The fox jumped over the fence";
        List<Pattern> patternList = new ArrayList<>();
        patternList.add(Pattern.compile("(th.)"));
        patternList.add(Pattern.compile("(he)"));
        patternList.add(Pattern.compile("(fox)"));
        List<String> result = PatternHelper.getCoincidenceList(patternList, chain);
        System.out.println(result);
        assertEquals(4, result.size());
        assertEquals("the",result.get(0));
        assertEquals("he",result.get(1));
        assertEquals("he",result.get(2));
        assertEquals("fox",result.get(3));
    }
    @Test
    public void testVariationF_nce() {
        String randomString = RandomStringUtils.random(1);
        String chain = "The fox jumped over the f"+randomString+"nce";
        List<Pattern> patternList = new ArrayList<>();
        patternList.add(Pattern.compile("(the)"));
        patternList.add(Pattern.compile("(fox|f.nce)"));
        List<String> result = PatternHelper.getCoincidenceList(patternList, chain);
        assertEquals(3, result.size());
        assertEquals("the",result.get(0));
        assertEquals("fox",result.get(1));
        assertEquals("f"+randomString+"nce",result.get(2));

    }

    @Test
    public void testNullChain() {
        expectedEx.expect(PatterException.class);
        expectedEx.expectMessage(PatterException.NULL_CHAIN);
        List<Pattern> patternList = new ArrayList<>();
        patternList.add(Pattern.compile("(the)"));
        patternList.add(Pattern.compile("(fox|f.nce)"));
        PatternHelper.getCoincidenceList(patternList, null);

    }

    @Test
    public void testEmptyChain() {
        expectedEx.expect(PatterException.class);
        expectedEx.expectMessage(PatterException.NULL_CHAIN);
        List<Pattern> patternList = new ArrayList<>();
        patternList.add(Pattern.compile("(the)"));
        patternList.add(Pattern.compile("(fox|f.nce)"));
        PatternHelper.getCoincidenceList(patternList, " ");

    }


    @Test
    public void testNoMatchPatterns() {
        List<Pattern> patternList = new ArrayList<>();
        patternList.add(Pattern.compile("(the)"));
        patternList.add(Pattern.compile("(fox|f.nce)"));
        List<String> result = PatternHelper.getCoincidenceList(patternList, "example");
        assertEquals(0,result.size());
    }

    @Test
    public void testNullPatterns() {
        assertEquals(0,PatternHelper.getCoincidenceList(null, "example").size());
    }


    @Test
    public void testComplexPatterns() {
        List<Pattern> patternList = new ArrayList<>();
        patternList.add(Pattern.compile("[A-Za-z0-9]{4}\\-[A-Za-z0-9]{4}"));
        patternList.add(Pattern.compile("(the)"));
        List<String> result = PatternHelper.getCoincidenceList(patternList, "the the arP9-12oP oi9i-LKJo/9875548");
        System.out.println(result.toString());
        assertEquals(4,result.size());
        assertEquals("arP9-12oP",result.get(0));
        assertEquals("oi9i-LKJo",result.get(1));
        assertEquals("the",result.get(2));
        assertEquals("the",result.get(3));
    }

    @Test
    public void testComplexPatterns_2() {
        List<Pattern> patternList = new ArrayList<>();
        patternList.add(Pattern.compile(".pattern.[0-9]"));
        patternList.add(Pattern.compile("(s..y)"));
        List<String> result = PatternHelper.getCoincidenceList(patternList, "sarysary sllywerwerpatterna9sadadsad13123ase asdpatternd8seey");
        assertEquals(6,result.size());
    }



    /**
     * NO REUSABLE
     */

    @Test
    public void testInitialTestNotReusable() {
        String chain = "The fox jumped over the fence";
        List<Pattern> patternList = new ArrayList<>();

        patternList.add(Pattern.compile("(th.)"));
        patternList.add(Pattern.compile("(he)"));
        patternList.add(Pattern.compile("(fox)"));

        List<String> result = PatternHelper.getCoincidenceListNotReusable(patternList, chain);
        assertEquals(3, result.size());
        assertEquals("the",result.get(0));
        assertEquals("he",result.get(1));
        assertEquals("fox",result.get(2));
    }

    @Test
    public void testInitialTestNotReusableDuplicado() {
        String chain = "the fox fox jumped over the fence fox he funce thereis";
        List<Pattern> patternList = new ArrayList<>();

        patternList.add(Pattern.compile("(th.)"));
        patternList.add(Pattern.compile("(he)"));
        patternList.add(Pattern.compile("(fox)"));

        List<String> result = PatternHelper.getCoincidenceListNotReusable(patternList, chain);

        assertEquals(3, result.size());
        assertEquals("the",result.get(0));
        assertEquals("he",result.get(1));
        assertEquals("fox",result.get(2));

    }

    @Test
    public void testNullChainNoReusable() {
        expectedEx.expect(PatterException.class);
        expectedEx.expectMessage(PatterException.NULL_CHAIN);
        List<Pattern> patternList = new ArrayList<>();
        patternList.add(Pattern.compile("(the)"));
        patternList.add(Pattern.compile("(fox|f.nce)"));
        PatternHelper.getCoincidenceListNotReusable(patternList, null);

    }

    @Test
    public void testEmptyChainNoReusable() {
        expectedEx.expect(PatterException.class);
        expectedEx.expectMessage(PatterException.NULL_CHAIN);
        List<Pattern> patternList = new ArrayList<>();
        patternList.add(Pattern.compile("(the)"));
        patternList.add(Pattern.compile("(fox|f.nce)"));
        PatternHelper.getCoincidenceListNotReusable(patternList, " ");

    }


    @Test
    public void testNoMatchPatternsNoReusabl() {
        List<Pattern> patternList = new ArrayList<>();
        patternList.add(Pattern.compile("(the)"));
        patternList.add(Pattern.compile("(fox|f.nce)"));
        List<String> result = PatternHelper.getCoincidenceListNotReusable(patternList, "example");
        assertEquals(0,result.size());
    }

    @Test
    public void testNullPatternsNoReusabl() {
        assertEquals(0,PatternHelper.getCoincidenceListNotReusable(null, "example").size());
    }



    @Test
    public void testComplexPatternsNoReusable() {
        List<Pattern> patternList = new ArrayList<>();
        patternList.add(Pattern.compile("[A-Za-z0-9]{4}\\-[A-Za-z0-9]{4}"));
        patternList.add(Pattern.compile("(the)"));
        List<String> result = PatternHelper.getCoincidenceListNotReusable(patternList, "the the arP9-12oP oi9i-LKJo/9875548 oi9i-LKJo/9875548asdqw134fad");

        assertEquals(3,result.size());
        assertEquals("arP9-12oP",result.get(0));
        assertEquals("oi9i-LKJo",result.get(1));
        assertEquals("the",result.get(2));
    }


    @Test
    public void testComplexPatternsNoReusable_2() {
        List<Pattern> patternList = new ArrayList<>();
        patternList.add(Pattern.compile(".pattern.[0-9]"));
        patternList.add(Pattern.compile("(s..y)"));
        /**
         * sary y slly aparecen dos veces en la cadena, al no poder reutilizar, el resultado muestra solo 1 de cada uno
         */
        List<String> result = PatternHelper.getCoincidenceListNotReusable(patternList, "sarysary sllywerwsllyerpatterna9sadadsad13123ase asdpatternd8seey");
        assertEquals(5,result.size());
        result = PatternHelper.getCoincidenceList(patternList, "sarysary sllywerwsllyerpatterna9sadadsad13123ase asdpatternd8seey");
        assertEquals(7,result.size());

    }

    /**
     * BLACKLIST
     */


    @Test
    public void testInitialTestNotReusableBlackList() {
        String chain = "The fox jumped over the fence";
        List<Pattern> patternList = new ArrayList<>();
        patternList.add(Pattern.compile("(the)"));
        patternList.add(Pattern.compile("(fox|f.nce)"));
        patternList.add(Pattern.compile("(abc)"));
        List<String> result = PatternHelper.getCoincidenceListNotReusableBlackList(patternList,
                chain,Pattern.compile("f.x"));
        System.out.println(result.toString());
        assertEquals(2, result.size());
        assertEquals("the",result.get(0));
        assertEquals("fence",result.get(1));
    }

    @Test
    public void testNullChainNoReusableBlackList() {
        expectedEx.expect(PatterException.class);
        expectedEx.expectMessage(PatterException.NULL_CHAIN);
        List<Pattern> patternList = new ArrayList<>();
        patternList.add(Pattern.compile("(the)"));
        patternList.add(Pattern.compile("(fox|f.nce)"));
        PatternHelper.getCoincidenceListNotReusableBlackList(patternList, null,
                Pattern.compile("f.x"));

    }

    @Test
    public void testEmptyChainNoReusableBlackList() {
        expectedEx.expect(PatterException.class);
        expectedEx.expectMessage(PatterException.NULL_CHAIN);
        List<Pattern> patternList = new ArrayList<>();
        patternList.add(Pattern.compile("(the)"));
        patternList.add(Pattern.compile("(fox|f.nce)"));
        PatternHelper.getCoincidenceListNotReusableBlackList(patternList, " ",
                Pattern.compile("f.x"));

    }


    @Test
    public void testNoMatchPatternsNoReusableBlackList() {
        List<Pattern> patternList = new ArrayList<>();
        patternList.add(Pattern.compile("(the)"));
        patternList.add(Pattern.compile("(fox|f.nce)"));
        List<String> result = PatternHelper.getCoincidenceListNotReusableBlackList(
                patternList, "example",Pattern.compile("f.x"));
        assertEquals(0,result.size());
    }

    @Test
    public void testNullPatternsNoReusableBlackList() {
        assertEquals(0,PatternHelper.getCoincidenceListNotReusableBlackList(
                null, "example",Pattern.compile("f.x")).size());
    }

    @Test
    public void testNullPatternsNoReusableBlackListNull() {
        expectedEx.expect(PatterException.class);
        expectedEx.expectMessage(PatterException.NULL_BLACKLIST);
        List<Pattern> patternList = new ArrayList<>();
        patternList.add(Pattern.compile("(the)"));
        patternList.add(Pattern.compile("(fox|f.nce)"));
        PatternHelper.getCoincidenceListNotReusableBlackList(patternList, "chain", null);
    }

    @Test
    public void testComplexPatternsNoReusableBlackList() {
        List<Pattern> patternList = new ArrayList<>();
        patternList.add(Pattern.compile(".pattern.[0-9]"));
        patternList.add(Pattern.compile("(s..y)"));
        /**
         * sary y slly aparecen dos veces en la cadena, al no poder reutilizar, el resultado muestra solo 1 de cada uno,
         * al estar incluidos tb en el patron black list, se eliminan de la lista
         */
        List<String> result = PatternHelper.getCoincidenceListNotReusableBlackList(patternList,
                "sarysary sllywerwsllyerpatterna9sadadsad13123ase asdpatternd8seey",
                Pattern.compile("[0-9]"));
        assertEquals(3,result.size());

    }
}

