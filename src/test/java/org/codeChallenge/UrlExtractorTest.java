package org.codeChallenge;

import org.apache.commons.lang.RandomStringUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import static org.junit.Assert.*;
import java.io.IOException;
import java.util.ArrayList;

public class UrlExtractorTest {


    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Test
    public void getResponseUrl_urlNull() throws IOException {
        expectedEx.expect(UrlExtractorException.class);
        expectedEx.expectMessage(UrlExtractorException.INCORRECT_URL);
        UrlExtractor.getResponseURL(null);
    }

    @Test
    public void getResponseUrl_incorrect() throws IOException {
        expectedEx.expect(UrlExtractorException.class);
        expectedEx.expectMessage(UrlExtractorException.INCORRECT_GET);
        UrlExtractor.getResponseURL(RandomStringUtils.random(10));
    }

    @Test
    public void getResponseUrl_correctBadResponseStatus() throws IOException {
        expectedEx.expect(UrlExtractorException.class);
        expectedEx.expectMessage(UrlExtractorException.INCORRECT_RESPONSE);
        UrlExtractor.getResponseURL("https://bitbucket.org/sooolfi/simios/src/master/");
    }


    @Test
    public void testPrintListNoResult() throws IOException {
        ArrayList<String> arrayDomains = new ArrayList<>();
        arrayDomains.add("0result.com");
        assertEquals(0,UrlExtractor.printAPTurlList(arrayDomains,UrlExtractor.URL_REPO_GIT).size());
    }

    @Test
    public void testPrintListWithResult() throws IOException {
        ArrayList<String> arrayDomains = new ArrayList<>();
        arrayDomains.add("cybereason.com");
        arrayDomains.add("symantec.com");
        assertTrue(UrlExtractor.printAPTurlList(arrayDomains,UrlExtractor.URL_REPO_GIT).size() > 0);
    }

    @Test
    public void testPrintListBadURLJson() throws IOException {
        expectedEx.expect(UrlExtractorException.class);
        expectedEx.expectMessage(UrlExtractorException.JSON_ERROR);
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("0result.com");
        UrlExtractor.printAPTurlList(arrayList,"https://api.github.com/repos/mitre/cti");
    }



}

